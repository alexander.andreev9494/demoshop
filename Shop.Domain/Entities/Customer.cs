using System;
using System.Collections.Generic;
using System.Linq;
using Shop.Domain.Enumerables;

namespace Shop.Domain.Entities
{
    public class Customer
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }

        public List<Order> Orders { get; set; }

        public void AddProductToCart(Product product)
        {
            var order = Orders.FirstOrDefault(o => o.Status == OrderStatus.Current);
            if (order == null)
                return;
            
            var orderItem = order.OrderItems?.FirstOrDefault(item => item.Product.Id == product.Id);
            if (orderItem != null)
            {
                orderItem.Quantity++;
                orderItem.RecalculatePrice();
            }
            else
            {
                order.OrderItems ??= new List<OrderItem>();
                
                order.OrderItems.Add(new OrderItem(product.Price)
                {
                    Product = product,
                    Quantity = 1
                });
            }
            
            order.CalculateTotalPrice();
        }

        public void ReduceQuantity(Product product)
        {
            var order = Orders.FirstOrDefault(o => o.Status == OrderStatus.Current);
            if (order == null)
                return;
            
            var orderItem = order.OrderItems?.FirstOrDefault(item => item.Product.Id == product.Id);
            if (orderItem != null)
            {
                orderItem.Quantity--;
                orderItem.RecalculatePrice();
            }

            order.CalculateTotalPrice();
        }

        public void RemoveProductFromCart(Product product)
        {
            var order = Orders.FirstOrDefault(o => o.Status == OrderStatus.Current);
            if (order != null)
                order.OrderItems = order.OrderItems.Where(item => item.Product.Id != product.Id).ToList();
        }

        public void AbandonCurrentOrder()
        {
            var order = Orders.FirstOrDefault(o => o.Status == OrderStatus.Current);

            order?.Abandon();
        }
    }
}