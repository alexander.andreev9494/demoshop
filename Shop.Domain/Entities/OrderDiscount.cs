using System;
using Shop.Domain.Entities;

namespace Shop.Domain.Entities
{
    public class OrderDiscount
    {
        public Guid OrderId { get; set; }
        public Order Order { get; set; }
        public Guid DiscountId { get; set; }
        public Discount Discount { get; set; }
    }
}