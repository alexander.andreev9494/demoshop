using System;
using System.Collections.Generic;
using System.Linq;
using Shop.Domain.Enumerables;
using Shop.Domain.Exceptions;

namespace Shop.Domain.Entities
{
    public class Order
    {
        public Guid Id { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public Customer Customer { get; set; }
        public OrderStatus Status { get; set; }
        public decimal TotalPrice { get; private set; }
        public List<OrderDiscount> OrderDiscounts { get; private set; }

        public void ApplyDiscount(Discount discount)
        {
            if (OrderDiscounts.Any(od => od.Discount.Code == discount.Code))
                throw new CanNotApplyDiscountException($"This discount code was already applied");
            
            ReducePriceForDiscount(discount);
            
            OrderDiscounts ??= new List<OrderDiscount>();
            
            OrderDiscounts.Add(new OrderDiscount
            {
                Discount = discount,
                Order = this
            });
        }

        public void CalculateTotalPrice()
        {
            TotalPrice = OrderItems.Sum(item => item.TotalPrice);

            var discounts = OrderDiscounts.Select(od => od.Discount);
            foreach (var discount in discounts)
                ReducePriceForDiscount(discount);
        }

        private void ReducePriceForDiscount(Discount discount)
        {
            if (discount.Type == DiscountType.Fixed)
                TotalPrice -= discount.Amount;
            else
                TotalPrice -= (TotalPrice / 100) * discount.Amount;
        }

        public void Abandon()
        {
            Status = OrderStatus.Abandoned;
        }
    }
}