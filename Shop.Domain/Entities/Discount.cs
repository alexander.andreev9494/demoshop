using System;
using System.Collections.Generic;
using Shop.Domain.Enumerables;

namespace Shop.Domain.Entities
{
    public class Discount
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public decimal Amount { get; set; }
        public DiscountType Type { get; set; }

        public List<OrderDiscount> OrderDiscounts { get; set; }
    }
}