namespace Shop.Domain.Enumerables
{
    public enum DiscountType
    {
        Fixed = 0,
        Percentage
    }
}