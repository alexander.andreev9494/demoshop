namespace Shop.Domain.Enumerables
{
    public enum OrderStatus
    {
        Current = 0,
        Submitted,
        Processed,
        Abandoned
    }
}