using System;
using System.Linq;
using Shop.Domain.Entities;
using Shop.Domain.Enumerables;
using Shop.Domain.Exceptions;

namespace Shop.Domain.UseCases
{
    public class OrderHandler
    {
        private IShopUoW ShopUoW { get; }

        public OrderHandler(IShopUoW shopUoW)
        {
            ShopUoW = shopUoW;
        }

        public Order Create(Guid customerId)
        {
            var order = ShopUoW.Orders.Get(o => o.Customer.Id == customerId && o.Status == OrderStatus.Current);
            if (order != null)
                throw new OrderExistsException($"Not submitted order for customer {customerId} already exists");

            order = new Order
            {
                Id = Guid.NewGuid(),
                Customer = ShopUoW.Customers.Get(c => c.Id == customerId),
                Status = OrderStatus.Current
            };

            ShopUoW.Orders.Add(order);
            ShopUoW.Commit();

            return order;
        }

        public void AbandonCurrentOrder(Guid customerId)
        {
            var customer = ShopUoW.Customers.Get(c => c.Id == customerId);
            customer.AbandonCurrentOrder();
            
            ShopUoW.Commit();
        }

        public void AbandonOrder(Guid orderId)
        {
            var order = ShopUoW.Orders.Get(o => o.Id == orderId);
            
            order?.Abandon();
            ShopUoW.Commit();
        }

        public Order ApplyDiscount(Guid customerId, string code)
        {
            var discount = ShopUoW.Discounts.Get(d => d.Code == code);
            if (discount == null)
                throw new DiscountCodeNotExists($"Discount code '{code}' not exists");
            
            var customer = ShopUoW.Customers.Get(c => c.Id == customerId);
            var order = customer.Orders.FirstOrDefault(o => o.Status == OrderStatus.Current);
            
            order?.ApplyDiscount(discount);
            ShopUoW.Commit();

            return order;
        }

        public void AddProductToCart(Guid productId, Guid customerId)
        {
            var product = ShopUoW.Products.Get(p => p.Id == productId);
            var customer = ShopUoW.Customers.Get(c => c.Id == customerId);
            
            customer.AddProductToCart(product);
            ShopUoW.Commit();
        }

        public void RemoveProductFromCart(Guid productId, Guid customerId)
        {
            var product = ShopUoW.Products.Get(p => p.Id == productId);
            var customer = ShopUoW.Customers.Get(c => c.Id == customerId);
            
            customer.RemoveProductFromCart(product);
            ShopUoW.Commit();
        }

        public void ReduceProductQuantity(Guid productId, Guid customerId)
        {
            var product = ShopUoW.Products.Get(p => p.Id == productId);
            var customer = ShopUoW.Customers.Get(c => c.Id == customerId);
            
            customer.ReduceQuantity(product);
            ShopUoW.Commit();
        }

        public Order CurrentCart(Guid customerId)
        {
            var customer = ShopUoW.Customers.Get(c => c.Id == customerId);
            var order = customer.Orders.FirstOrDefault(o => o.Status == OrderStatus.Current);

            return order;
        }
    }
}