using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Shop.Domain.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Get(Guid id);
        TEntity Get(Expression<Func<TEntity, bool>> condition);
        IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> condition);
        IEnumerable<TEntity> GetPaginated(int pageNumber, int pageSize);
        long Count();
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(TEntity entity);
    }
}