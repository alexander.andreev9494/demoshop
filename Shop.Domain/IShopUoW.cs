using Shop.Domain.Entities;
using Shop.Domain.Repositories;

namespace Shop.Domain
{
    public interface IShopUoW
    {
        IRepository<Customer> Customers { get; set; }
        IRepository<Discount> Discounts { get; set; }
        IRepository<OrderItem> OrderItems { get; set; }
        IRepository<Order> Orders { get; set; }
        IRepository<Product> Products { get; set; }

        void Commit();
    }
}