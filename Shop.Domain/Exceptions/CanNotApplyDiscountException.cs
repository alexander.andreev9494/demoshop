using System;

namespace Shop.Domain.Exceptions
{
    public class CanNotApplyDiscountException : Exception
    {
        public CanNotApplyDiscountException(string message) : base(message)
        {
            
        }
    }
}