using System;

namespace Shop.Domain.Exceptions
{
    public class OrderExistsException : Exception
    {
        public OrderExistsException(string message) : base(message) { }
    }
}