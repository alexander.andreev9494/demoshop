using System;

namespace Shop.Domain.Exceptions
{
    public class DiscountCodeNotExists : Exception
    {
        public DiscountCodeNotExists(string message) : base(message)
        {
            
        }
    }
}