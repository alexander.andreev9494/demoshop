using System;
using System.Collections.Generic;
using System.Linq;
using Shop.Domain;
using Shop.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Shop.StoreApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        public IShopUoW ShopUoW { get; }

        public ProductController(IShopUoW shopUoW)
        {
            ShopUoW = shopUoW;
        }

        [HttpGet("Paginated")]
        public IActionResult GetPaginated([FromQuery] int pageNumber, [FromQuery] int pageSize)
        {
            var products = ShopUoW.Products.GetPaginated(pageNumber, pageSize).ToArray();
            
            Response.Headers.Add("X-Total-Count", ShopUoW.Products.Count().ToString());
            Response.Headers.Add("X-Count", products.Length.ToString());
            
            return Ok(products);
        }

        [HttpGet("{id}")]
        public IActionResult GetOne([FromRoute] Guid id)
        {
            var product = ShopUoW.Products.Get(id);
            if (product == null)
                return NotFound();
            
            return Ok(product);
        }
    }
}