using System;
using Shop.Domain;
using Shop.Domain.Entities;
using Shop.Domain.UseCases;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Shop.StoreApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {
        public IShopUoW ShopUoW { get; }
        public OrderHandler OrderHandler { get; }

        public OrderController(IShopUoW shopUoW, OrderHandler orderHandler)
        {
            ShopUoW = shopUoW;
            OrderHandler = orderHandler;
        }

        
        [HttpPost("Create")]
        public IActionResult CreateOrder()
        {
            var customerId = Guid.Parse("82f61fbd-a1fc-4683-ba53-6cfa202709be");

            var order = OrderHandler.Create(customerId);
            return Ok(order);
        }

        
        [HttpPost("AddProductToOrder/{id}")]
        public IActionResult AddProductToOrder([FromRoute] Guid id)
        {
            var customerId = Guid.Parse("82f61fbd-a1fc-4683-ba53-6cfa202709be");
            OrderHandler.AddProductToCart(id, customerId);

            return Ok();
        }

        [HttpPut("ApplyDiscountToOrder/{code}")]
        public IActionResult ApplyDiscountToOrder([FromRoute] string code)
        {
            var customerId = Guid.Parse("82f61fbd-a1fc-4683-ba53-6cfa202709be");
            var order = OrderHandler.ApplyDiscount(customerId, code);

            return Ok(order);
        }
        
        [HttpDelete("RemoveProductFromOrder/{id}")]
        public IActionResult RemoveProductFromOrder([FromRoute] Guid id)
        {
            //var customerId = Guid.Parse(HttpContext.User.Identity.Name);
            var customerId = Guid.Parse("82f61fbd-a1fc-4683-ba53-6cfa202709be");
            OrderHandler.RemoveProductFromCart(id, customerId);

            return Ok();
        }

        [HttpGet("CurrentCart")]
        public IActionResult GetCurrentCart()
        {
            var customerId = Guid.Parse("82f61fbd-a1fc-4683-ba53-6cfa202709be");
            return Ok(OrderHandler.CurrentCart(customerId));
        }

        [HttpPut("ReduceProductQuantity/{id}")]
        public IActionResult ReduceProductQuantity([FromRoute] Guid id)
        {
            var customerId = Guid.Parse("82f61fbd-a1fc-4683-ba53-6cfa202709be");
            OrderHandler.ReduceProductQuantity(id, customerId);

            return Ok();
        }
    }
}