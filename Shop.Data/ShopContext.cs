using System;
using System.Linq;
using Shop.Data.Repositories;
using Shop.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace Shop.Data
{
    public class ShopContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Discount> Discounts { get; set; }
        public DbSet<OrderDiscount> OrderDiscounts { get; set; }
        public DbSet<Product> Products { get; set; }

        public ShopContext()
        {
            
        }

        public ShopContext(DbContextOptions options) : base(options)
        {
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseNpgsql(config.GetConnectionString("DefaultConnection"));
        }

        public override int SaveChanges()
        {
            GenerateIds();
            return base.SaveChanges();
        }

        private void GenerateIds()
        {
            foreach (var entityEntry in ChangeTracker.Entries())
            {
                if (entityEntry.State != EntityState.Added)
                    continue;
                
                var type = entityEntry.Entity.GetType();
                
                var id = Guid.NewGuid();
                type.GetProperty("Id")?.SetValue(entityEntry.Entity, id);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderDiscount>()
                .HasKey(od => new {od.DiscountId, od.OrderId});

            modelBuilder.Entity<OrderDiscount>()
                .HasOne<Order>(od => od.Order)
                .WithMany(o => o.OrderDiscounts);

            modelBuilder.Entity<OrderDiscount>()
                .HasOne(od => od.Discount)
                .WithMany(d => d.OrderDiscounts);
        }
    }
}