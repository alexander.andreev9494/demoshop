using System;
using System.Reflection;
using Shop.Data.Repositories;
using Shop.Domain;
using Shop.Domain.Entities;
using Shop.Domain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Shop.Data
{
    public class ShopUoW : IShopUoW
    {
        private ShopContext ShopContext { get; }
        
        public IRepository<Customer> Customers { get; set; }
        public IRepository<Discount> Discounts { get; set; }
        public IRepository<OrderItem> OrderItems { get; set; }
        public IRepository<Order> Orders { get; set; }
        public IRepository<Product> Products { get; set; }

        public ShopUoW(ShopContext shopContext)
        {
            ShopContext = shopContext;
            Customers = new CustomerRepository(ShopContext);
            Discounts = new Repository<Discount>(ShopContext);
            OrderItems = new Repository<OrderItem>(ShopContext);
            Orders = new OrderRepository(ShopContext);
            Products = new Repository<Product>(ShopContext);
        }
        
        public void Commit()
        {
            ShopContext.SaveChanges();
        }
    }
}