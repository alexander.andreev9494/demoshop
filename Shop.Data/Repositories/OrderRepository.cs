using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Shop.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Shop.Data.Repositories
{
    public class OrderRepository : Repository<Order>
    {
        public OrderRepository(ShopContext context) : base(context)
        {
        }

        public override Order Get(Guid id)
        {
            var order = Context.Orders.Include(o => o.OrderDiscounts)
                .ThenInclude(od => od.Discount)
                .FirstOrDefault(o => o.Id == id);

            return order;
        }

        public override Order Get(Expression<Func<Order, bool>> condition)
        {
            var order = Context.Orders.Include(o => o.OrderDiscounts)
                .ThenInclude(od => od.Discount)
                .FirstOrDefault(condition);

            return order;
        }

        public override IEnumerable<Order> GetMany(Expression<Func<Order, bool>> condition)
        {
            var orders = Context.Orders.Include(o => o.OrderDiscounts)
                .ThenInclude(od => od.Discount)
                .Where(condition)
                .ToArray();
            
            return orders;
        }
    }
}