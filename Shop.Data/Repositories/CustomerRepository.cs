using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Shop.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Shop.Data.Repositories
{
    public class CustomerRepository : Repository<Customer>
    {
        public CustomerRepository(ShopContext context) : base(context)
        {
        }

        public override Customer Get(Expression<Func<Customer, bool>> condition)
        {
            return Context.Customers.Include(c => c.Orders)
                .ThenInclude(o => o.OrderItems)
                .ThenInclude(oi => oi.Product)
                .Include(c => c.Orders)
                .ThenInclude(o => o.OrderDiscounts)
                .ThenInclude(od => od.Discount)
                .FirstOrDefault(condition);
        }

        public override IEnumerable<Customer> GetMany(Expression<Func<Customer, bool>> condition)
        {
            return Context.Customers
                .Include(c => c.Orders)
                .ThenInclude(o => o.OrderItems)
                .ThenInclude(oi => oi.Product)
                .Include(c => c.Orders)
                .ThenInclude(o => o.OrderDiscounts)
                .ThenInclude(od => od.Discount)
                .Where(condition)
                .ToArray();
        }
    }
}