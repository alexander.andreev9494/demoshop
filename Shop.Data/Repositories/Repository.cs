using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Shop.Domain.Repositories;

namespace Shop.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected ShopContext Context { get; }

        public Repository(ShopContext context)
        {
            Context = context;
        }

        public virtual TEntity Get(Guid id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        public virtual TEntity Get(Expression<Func<TEntity, bool>> condition)
        {
            return Context.Set<TEntity>().FirstOrDefault(condition);
        }

        public virtual IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> condition)
        {
            return Context.Set<TEntity>().Where(condition);
        }

        public IEnumerable<TEntity> GetPaginated(int pageNumber, int pageSize)
        {
            return Context.Set<TEntity>()
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .ToArray();
        }

        public long Count()
        {
            return Context.Set<TEntity>().Count();
        }

        public virtual void Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
        }

        public virtual void Update(TEntity entity)
        {
            Context.Set<TEntity>().Update(entity);
        }

        public virtual void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }
    }
}