using System.ComponentModel.DataAnnotations;

namespace Shop.StoreFront.Requests
{
    public class ApplyDiscountRequest
    {
        [Required]
        [MinLength(7)]
        public string Code { get; set; }
    }
}