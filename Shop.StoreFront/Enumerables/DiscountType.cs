namespace Shop.StoreFront.Enumerables
{
    public enum DiscountType
    {
        Fixed = 0,
        Percentage
    }
}