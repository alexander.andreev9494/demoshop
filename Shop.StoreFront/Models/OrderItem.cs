using System;

namespace Shop.StoreFront.Models
{
    public class OrderItem
    {
        public Guid Id { get; set; }
        public Order Order { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; private set; }
        public decimal TotalPrice { get; set; }

        public OrderItem(decimal price)
        {
            TotalPrice = Price = price;
        }
        
        public void RecalculatePrice()
        {
            TotalPrice = Price * Quantity;
        }
    }
}