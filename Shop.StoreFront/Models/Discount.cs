using System;
using System.Collections.Generic;
using Shop.StoreFront.Enumerables;

namespace Shop.StoreFront.Models
{
    public class Discount
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public decimal Amount { get; set; }
        public DiscountType Type { get; set; }

        public List<Order> Orders { get; set; }
    }
}