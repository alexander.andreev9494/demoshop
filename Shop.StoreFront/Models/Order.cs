using System;
using System.Collections.Generic;
using System.Linq;
using Shop.StoreFront.Enumerables;

namespace Shop.StoreFront.Models
{
    public class Order
    {
        public Guid Id { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public Customer Customer { get; set; }
        public OrderStatus Status { get; set; }
        public decimal TotalPrice { get; set; }
        public List<Discount> Discounts { get; set; }
        public List<OrderDiscount> OrderDiscounts { get; set; }
        
        public void CalculateTotalPrice()
        {
            TotalPrice = OrderItems.Sum(item => item.TotalPrice);
            
            var discounts = OrderDiscounts.Select(od => od.Discount);
            foreach (var discount in discounts)
                ReducePriceForDiscount(discount);
        }

        private void ReducePriceForDiscount(Discount discount)
        {
            if (discount.Type == DiscountType.Fixed)
                TotalPrice -= discount.Amount;
            else
                TotalPrice -= (TotalPrice / 100) * discount.Amount;
        }
    }
}